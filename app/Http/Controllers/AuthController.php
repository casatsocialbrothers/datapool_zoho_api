<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Post;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    //creates auth token
    public function auth()
    {
        if(!env('PRODUCTION')){
            $response = Http::post(
                'https://accounts.zoho.eu/apiauthtoken/nb/create', 
                [
                    'SCOPE' => 'ZohoRecruit/recruitapi',
                    'EMAIL_ID' => env('EMAIL_ID'),
                    'PASSWORD'=> env('PASSWORD')
                ]
            );

            echo ($response);
            
        }
    }
}
