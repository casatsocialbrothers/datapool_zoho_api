<?php

namespace App\Http\Controllers;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function get()
    {
        try
        {
            $ins=ZCRMRestClient::getInstance();
            $moduleArr=$ins->getAllModules()->getData();
            foreach ($moduleArr as $module)
            {
                echo "ModuleName:".$module->getModuleName();
                echo "SingLabel:".$module->getSingularLabel();
                echo "PluLabel:".$module->getPluralLabel();
                echo "BusinesscardLimit:".$module->getBusinessCardFieldLimit();
                echo "ApiName:".$module->getAPIName();
                $fields=$module->getFields();
                if($fields==null)
                {
                    continue;
                }
                foreach ($fields as $field)
                {
                    echo $field->getApiName().", ";
                    echo $field->getLength().", ";
                    echo $field->IsVisible().", ";
                    echo $field->getFieldLabel().", ";
                    echo $field->getCreatedSource().", ";
                    echo $field->isMandatory().", ";
                    echo $field->getSequenceNumber().", ";
                    echo $field->isReadOnly().", ";
                    echo $field->getDataType().", ";
                    echo $field->getId().", ";
                    echo $field->isCustomField().", ";
                    echo $field->isBusinessCardSupported().", ";
                    echo $field->getDefaultValue().", ";
                }
            }
        }
        catch (ZCRMException $e)
        {
            echo $e->getCode();
            echo $e->getMessage();
            echo $e->getExceptionCode();
        }
    echo "test";

    }
    //
}
