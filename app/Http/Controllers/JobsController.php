<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Post;

class JobsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    //creates auth token
    private function auth()
    {
        $response = Http::get(
            'https://accounts.zoho.eu/apiauthtoken/nb/create', 
            [
                'SCOPE' => 'ZohoRecruit/recruitapi',
                'EMAIL_ID' => env('EMAIL_ID'),
                'PASSWORD'=> env('PASSWORD')
            ]
        );

        if(env('DEBUG'))
        {
            var_dump(json_decode($response));
        }

        if(isset($response['AUTHTOKEN']))
        {
            return ['result'=>true, 'data'=>$response['AUTHTOKEN']];
        }
        return ['result'=>false];
        
    }

    //checks the response for errors
    private function validate_response($response)
    {
        if(env('DEBUG'))
        {
            var_dump($response);
        }
        if(isset($response['response']['error']))
        {
            return false;
        }
        return true;
    }

    //gets all job offers
    public function get($retry=true)
    {
        $dirty_response = Http::get(
            'https://recruit.zoho.eu/recruit/private/json/JobOpenings/getRecords', 
            [
                'authtoken' => env('AUTH_TOKEN'),
                'scope' => 'recruitapi',
                'version'=>2
            ]
        );
        $dirty_response = json_decode($dirty_response, true);
        $response = [];

        $vac_number=0;
        if($this->validate_response($dirty_response)){   
            foreach($dirty_response['response']['result']['JobOpenings']['row'] as $row)
            {
                foreach($row["FL"] as $item)
                {
                    $info[$item['val']]=$item['content'];
                }
                $response[]=$info;
                $vac_number++;
            }
            echo json_encode(['count'=>$vac_number, 'jobs'=>$response], JSON_PRETTY_PRINT);
        }
    }

    //get one job offer
    public function getone($id, $retry=true)
    {
        $dirty_response = Http::get(
            'https://recruit.zoho.eu/recruit/private/json/JobOpenings/getRecordById', 
            [
                'id'=>$id,
                'authtoken' => env('AUTH_TOKEN'),
                'scope' => 'recruitapi',
                'version'=>2
            ]
        );
        $response = [];

        $dirty_response = json_decode($dirty_response, true);
        if($this->validate_response($dirty_response))
        {
            foreach($dirty_response['response']['result']['JobOpenings'] as $row)
            {
                foreach($row["FL"] as $item)
                {
                    $info[$item['val']]=$item['content'];
                }
                $response[]=$info;
            }
        }

        echo json_encode($response, JSON_PRETTY_PRINT);
    }
}
