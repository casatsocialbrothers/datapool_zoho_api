<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// Get all jobs openings
$router->get('/get', "JobsController@get");

// Get one job opening by id
$router->get('/get/{id}', "JobsController@getone");

// Generate auth token
$router->get('/auth', "AuthController@auth");