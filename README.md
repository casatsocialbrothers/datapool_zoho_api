#######################
#### DOCUMENTATION ####
#######################

# Documentation on ZoHo Api
https://help.zoho.com/portal/en/kb/recruit/developer-guide

# useful alternative 
https://docs.wso2.com/display/ESBCONNECTORS/Working+with+Records+in+Zoho+Recruit#WorkingwithRecordsinZohoRecruit-getRecords

Be sure to turn off DEBUG in production :^)

# Instructions
To get all job offerings you go to {{host}}/get, to get a specific job you use {{host}}/get/{job_offering_id}.

You can find a sample response in get_examples.txt

In order to Authenticate you'll need to set "AUTH_TOKEN" in the .env to that of an API token connected to the client, the AuthControllers allows you to generate a new token if the old one got deleted.

# Endpoints
{{host}}/get         Get all job offerings
{{host}}/get/{id}     Get job offering by id

# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Contributing

Thank you for considering contributing to Lumen! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
